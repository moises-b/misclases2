/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package bomba;

/**
 *
 * @author moise
 */
public class Bomba {
    //Atributos
    private int idBomba;
    private int ContadorDeLitros;
    private int capacidad;
    private Gasolina gasolina;
    
    
    //Constructores
    
   public Bomba(){
       this.idBomba = 0;
       this.ContadorDeLitros = 0;
       this.capacidad = 0;
       this.gasolina = new Gasolina();
        
    }
   

    public Bomba(int idBomba, int contadorLitros, int capacidad, Gasolina gasolina) {
        this.idBomba = idBomba;
        this.ContadorDeLitros = contadorLitros;
        this.capacidad = capacidad;
        this.gasolina = gasolina;
    }
    
    
    //Encapsulamiento

    public int getIdBomba() {
        return idBomba;
    }

    public void setIdBomba(int idBomba) {
        this.idBomba = idBomba;
    }

    public int getContadorDeLitros() {
        return ContadorDeLitros;
    }

    public void setContadorDeLitros(int contador) {
        this.ContadorDeLitros = contador;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    public Gasolina getGasolina() {
        return gasolina;
    }

    public void setGasolina(Gasolina gasolina) {
        this.gasolina = gasolina;
    }
    
    
    
    
     //comportamiento
            public float obtenerInventario(){
                return this.capacidad- this.ContadorDeLitros;
            }
            
            public float venderGasolina ( int cantidad){
                float totalVenta =0.0f;
                
                if(cantidad <= this.obtenerInventario()){
                    totalVenta =cantidad*this.gasolina.getPrecio();
                    this.ContadorDeLitros = this.ContadorDeLitros + cantidad;
                }
                return totalVenta;
            }
            
            public float totalVenta(){
                return this.ContadorDeLitros * this.gasolina.getPrecio();
            }
            
    
}
