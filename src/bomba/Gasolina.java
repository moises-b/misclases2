/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package bomba;

/**
 *
 * @author moise
 */
public class Gasolina {
    //atributos
    private int idGasolina;
    private String tipo;
    private String marca;
    private float precio;

    
    
    
    //Contructores
    
    public Gasolina(){
        this.idGasolina = 0;
        this.tipo = "";
        this.marca = "";
        this.precio = 0.0f;
        
    }

    public Gasolina(int idGasolina, String tipo, String marca, float precio) {
        this.idGasolina = idGasolina;
        this.tipo = tipo;
        this.marca = marca;
        this.precio = precio;
    }
    
    
    
    //Encapsulamiento
    
    public int getIdGasolina() {
        return idGasolina;
    }

    public void setIdGasolina(int idGasolina) {
        this.idGasolina = idGasolina;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
    
    
    //comportamiento
    public String obtenerInformacion(){
        return "id :" + this.idGasolina + "Marca:" +
                this.marca + "Tipo:" + 
                this.tipo + "precio" + this.precio;
    }
    
            
}
