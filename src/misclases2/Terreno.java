
package misclases2;

/**
 *
 * @author moise
 */
public class Terreno {
    private int NumLote;
    private float Ancho;
    private float Largo;

    
    //constructores
    
    //de parametros
    public Terreno( int NumLote, float Ancho, float Largo){
        this.NumLote = NumLote;
        this.Ancho = Ancho;
        this.Largo = Largo;
        
    }
    //vacio
     public Terreno(){
       this.NumLote = 0;
       this. Ancho = 0.0f;
       this.Largo = 0.0f;
     }
     
     
     
     //copia
     public Terreno(Terreno t){
       this.NumLote = t.NumLote;
       this. Ancho = t.Ancho;
       this.Largo = t.Largo;
     }
     
     
     
     
//Encapsulamiento

    public int getNumLote() {
        return NumLote;
    }

    public void setNumLote(int NumLote) {
        this.NumLote = NumLote;
    }

    public float getAncho() {
        return Ancho;
    }

    public void setAncho(float Ancho) {
        this.Ancho = Ancho;
    }

    public float getLargo() {
        return Largo;
    }

    public void setLargo(float Largo) {
        this.Largo = Largo;
    }
    
    //metodos de comportamiento
    public float calcularPerimetro(){
        return this.Ancho * 2+ this.Largo;
    }
    public float calculoArea(){
        return this.Ancho * this.Largo;
    }
    
    
}
