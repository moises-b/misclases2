/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Herencia;

/**
 *
 * @author moise
 */
public class Base extends Empleados implements impuestos{
    
    private float pagoDiarios;
    private float diasTrabajados;

    public Base(float pagoDiarios, float diasTrabajados, int numEmpleado, String nombre, String puesto, String depto) {
        super(numEmpleado, nombre, puesto, depto);
        this.pagoDiarios = pagoDiarios;
        this.diasTrabajados = diasTrabajados;
    }

    public Base() {
        super();
        this.pagoDiarios=0.0f;
        this.diasTrabajados=0.0f;
    }

    public float getPagoDiarios() {
        return pagoDiarios;
    }

    public void setPagoDiarios(float pagoDiarios) {
        this.pagoDiarios = pagoDiarios;
    }

    public float getDiasTrabajados() {
        return diasTrabajados;
    }

    public void setDiasTrabajados(float diasTrabajados) {
        this.diasTrabajados = diasTrabajados;
    }
    
    

    @Override
    public float calcularPago() {
        return this.diasTrabajados * this.pagoDiarios;
    }



    @Override
    public float calcularImpuestos() {
        float pago= 0;
        if(this.calcularPago()>15000) pago= this.calcularPago()*.15f;
        return pago;
    }


}
