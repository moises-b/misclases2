/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JInternalFrame.java to edit this template
 */
package Herencia;

import javax.swing.JOptionPane;

/**
 *
 * @author moise
 */
public class jifEmpleadosBase extends javax.swing.JInternalFrame {

    /**
     * Creates new form jifEmpleadosBase
     */
    public jifEmpleadosBase() {
        initComponents();
        this.resize(400,500);
        this.desahabilitar();
        
    }
    public void desahabilitar(){
        this.txtnumEmpleados.setEnabled(false);
        this.txtNombre.setEnabled(false);
        this.txtPuesto.setEnabled(false);
        this.txtDepartamento.setEnabled(false);
        this.txtPagoDiario.setEnabled(false);
        this.txtDiasTrabajados.setEnabled(false);
        this.txtImpuesto.setEnabled(false);
        this.txtSubtotal.setEnabled(false);
        this.txtTotal.setEnabled(false);
        this.btnMostrar.setEnabled(false);
        this.btnGuardar.setEnabled(false);
        this.btnCancelar.setEnabled(false);
        this.btnLimpiar.setEnabled(false);
        this.btnCerrar.setEnabled(false);
    }
    public void habilitar(){
        this.txtnumEmpleados.setEnabled(true);
        this.txtNombre.setEnabled(true);
        this.txtPuesto.setEnabled(true);
        this.txtDepartamento.setEnabled(true);
        this.txtPagoDiario.setEnabled(true);
        this.txtDiasTrabajados.setEnabled(true);
        
        this.btnMostrar.setEnabled(true);
        this.btnGuardar.setEnabled(true);
        this.btnCancelar.setEnabled(true);
        this.btnLimpiar.setEnabled(true);
        this.btnCerrar.setEnabled(true);
    }
    public boolean validar(){
        boolean exito = true;
        
        if(this.txtnumEmpleados.getText().equals(""))exito=false;
        if(this.txtNombre.getText().equals("")) exito=false;
        if(this.txtPuesto.getText().equals("")) exito=false;
        if(this.txtDepartamento.getText().equals("")) exito=false;
        if(this.txtPagoDiario.getText().equals("")) exito=false;
        if(this.txtDiasTrabajados.getText().equals("")) exito=false;
        
        return exito;
    }
    public void limpiar(){
        this.txtnumEmpleados.setText("");
        this.txtnumEmpleados.requestFocus();
        this.txtNombre.setText("");
        this.txtPuesto.setText("");
        this.txtPagoDiario.setText("");
        this.txtDiasTrabajados.setText("");
        this.txtDepartamento.setText("");
        this.txtSubtotal.setText("");
        this.txtTotal.setText("");
        this.txtImpuesto.setText("");
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtPagoDiario = new javax.swing.JTextField();
        txtnumEmpleados = new javax.swing.JTextField();
        txtNombre = new javax.swing.JTextField();
        txtDepartamento = new javax.swing.JTextField();
        txtPuesto = new javax.swing.JTextField();
        txtDiasTrabajados = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        btnCancelar = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnMostrar = new javax.swing.JButton();
        btnCerrar = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        txtSubtotal = new javax.swing.JTextField();
        txtImpuesto = new javax.swing.JTextField();
        txtTotal = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setTitle("EmpleadoBase");
        getContentPane().setLayout(null);

        jLabel1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel1.setText("Pago Diario:");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(50, 190, 100, 15);

        jLabel2.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel2.setText("NumEmpleado:");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(50, 40, 110, 17);

        jLabel4.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel4.setText("Dias Trabajados:");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(50, 160, 100, 15);

        jLabel5.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel5.setText("Nombre:");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(50, 70, 80, 15);

        jLabel6.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel6.setText("Departamento:");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(50, 100, 100, 15);
        getContentPane().add(txtPagoDiario);
        txtPagoDiario.setBounds(130, 190, 80, 22);

        txtnumEmpleados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnumEmpleadosActionPerformed(evt);
            }
        });
        getContentPane().add(txtnumEmpleados);
        txtnumEmpleados.setBounds(150, 40, 80, 22);

        txtNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNombreActionPerformed(evt);
            }
        });
        getContentPane().add(txtNombre);
        txtNombre.setBounds(100, 70, 80, 22);

        txtDepartamento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDepartamentoActionPerformed(evt);
            }
        });
        getContentPane().add(txtDepartamento);
        txtDepartamento.setBounds(140, 100, 80, 22);

        txtPuesto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPuestoActionPerformed(evt);
            }
        });
        getContentPane().add(txtPuesto);
        txtPuesto.setBounds(100, 130, 80, 22);
        getContentPane().add(txtDiasTrabajados);
        txtDiasTrabajados.setBounds(150, 160, 80, 22);

        jLabel8.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel8.setText("Puesto:");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(50, 130, 80, 15);

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCancelar);
        btnCancelar.setBounds(240, 340, 80, 40);

        btnNuevo.setText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        getContentPane().add(btnNuevo);
        btnNuevo.setBounds(260, 60, 80, 40);

        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        getContentPane().add(btnGuardar);
        btnGuardar.setBounds(260, 120, 80, 40);

        btnMostrar.setText("Mostrar");
        btnMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnMostrar);
        btnMostrar.setBounds(260, 190, 80, 40);

        btnCerrar.setText("Cerrar");
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCerrar);
        btnCerrar.setBounds(140, 340, 80, 40);

        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });
        getContentPane().add(btnLimpiar);
        btnLimpiar.setBounds(30, 340, 80, 40);
        getContentPane().add(txtSubtotal);
        txtSubtotal.setBounds(110, 230, 64, 22);
        getContentPane().add(txtImpuesto);
        txtImpuesto.setBounds(110, 270, 64, 22);

        txtTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalActionPerformed(evt);
            }
        });
        getContentPane().add(txtTotal);
        txtTotal.setBounds(120, 310, 64, 22);

        jLabel9.setText("Subtotal:");
        getContentPane().add(jLabel9);
        jLabel9.setBounds(40, 230, 62, 16);

        jLabel10.setText("Impuesto:");
        getContentPane().add(jLabel10);
        jLabel10.setBounds(40, 270, 53, 16);

        jLabel11.setText("Total a Pagar:");
        getContentPane().add(jLabel11);
        jLabel11.setBounds(40, 310, 71, 16);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtnumEmpleadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnumEmpleadosActionPerformed

    }//GEN-LAST:event_txtnumEmpleadosActionPerformed

    private void txtNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNombreActionPerformed

    private void txtTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalActionPerformed

    private void txtDepartamentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDepartamentoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDepartamentoActionPerformed

    private void txtPuestoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPuestoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPuestoActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        // TODO add your handling code here:
        this.habilitar();
        this.txtnumEmpleados.requestFocus();
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        // TODO add your handling code here:
if(this.validar() == true) {

        base.setNumEmpleados(Integer.parseInt(this.txtnumEmpleados.getText()));
        base.setNombre(this.txtNombre.getText());
        base.setDepto(this.txtDepartamento.getText());
        base.setPuesto(this.txtPuesto.getText());
        base.setDiasTrabajados(Float.parseFloat(this.txtDiasTrabajados.getText()));
        base.setPagoDiarios(Float.parseFloat(this.txtPagoDiario.getText()));
        JOptionPane.showMessageDialog(this, "Se guardó con éxito");
        this.limpiar();
        this.btnMostrar.setEnabled(true);
} else {
    JOptionPane.showMessageDialog(this, "Faltaron datos por capturar");
}

    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        // TODO add your handling code here:
         this.limpiar();
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void btnMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarActionPerformed
        // TODO add your handling code here:
        this.txtSubtotal.setText(String.valueOf(base.calcularPago()));
        this.txtImpuesto.setText(String.valueOf(base.calcularImpuestos()));
        this.txtTotal.setText(String.valueOf(base.calcularPago()- base.calcularImpuestos()));
        //operaciones
    }//GEN-LAST:event_btnMostrarActionPerformed

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        // TODO add your handling code here:
       int res= JOptionPane.showConfirmDialog(this,"Desea salir","Empleados",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
        if (res == JOptionPane.YES_OPTION) this.dispose();
        
    }//GEN-LAST:event_btnCerrarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnCerrar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnMostrar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField txtDepartamento;
    private javax.swing.JTextField txtDiasTrabajados;
    private javax.swing.JTextField txtImpuesto;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtPagoDiario;
    private javax.swing.JTextField txtPuesto;
    private javax.swing.JTextField txtSubtotal;
    private javax.swing.JTextField txtTotal;
    private javax.swing.JTextField txtnumEmpleados;
    // End of variables declaration//GEN-END:variables
private Base base= new Base();
}
