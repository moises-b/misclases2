/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Herencia;

/**
 *
 * @author moise
 */
public abstract class Empleados {
    protected int numEmpleados=0;
    protected String nombre="";
    protected String puesto="";
    protected String depto="";
    //constructores

    public Empleados(int numEmpleado, String nombre,String puesto, String depto) {
        this.numEmpleados=numEmpleado;
        this.nombre=nombre;
        this.puesto=puesto;
        this.depto=depto;
    }
    public Empleados(){
    this.numEmpleados=0;
    this.nombre="";
    this.puesto="";
    this.depto="";
    }

    public int getNumEmpleados() {
        return numEmpleados;
    }

    public void setNumEmpleados(int numEmpleados) {
        this.numEmpleados = numEmpleados;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public String getDepto() {
        return depto;
    }

    public void setDepto(String depto) {
        this.depto = depto;
    }
    public abstract float calcularPago();
    
}
