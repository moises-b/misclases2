/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Herencia;

/**
 *
 * @author moise
 */
public class Eventual extends Empleados{
    
    private float pagoHoras;
    private float horasTrabajadas;

    public Eventual(float pagoHoras, float horasTrabajadas, int numEmpleado, String nombre, String puesto, String depto) {
        super(numEmpleado, nombre, puesto, depto);
        this.pagoHoras = pagoHoras;
        this.horasTrabajadas = horasTrabajadas;
    }

    public Eventual() {
        super();
        this.pagoHoras = 0.0f;
        this.horasTrabajadas = 0.0f;
    }

    public float getPagoHoras() {
        return pagoHoras;
    }

    public void setPagoHoras(float pagoHoras) {
        this.pagoHoras = pagoHoras;
    }

    public float getHorasTrabajadas() {
        return horasTrabajadas;
    }

    public void setHorasTrabajadas(float horasTrabajadas) {
        this.horasTrabajadas = horasTrabajadas;
    }
    
    

    @Override
    public float calcularPago() {
       float pago= 0.0f;
       pago=this.horasTrabajadas*pagoHoras;
       return pago;
    }
    
}
